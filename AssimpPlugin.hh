/*===========================================================================*\
*                                                                            *
*                              OpenFlipper                                   *
*      Copyright (C) 2001-2011 by Computer Graphics Group, RWTH Aachen       *
*                           www.openflipper.org                              *
*                                                                            *
*--------------------------------------------------------------------------- *
*  This file is part of OpenFlipper.                                         *
*                                                                            *
*  OpenFlipper is free software: you can redistribute it and/or modify       *
*  it under the terms of the GNU Lesser General Public License as            *
*  published by the Free Software Foundation, either version 3 of            *
*  the License, or (at your option) any later version with the               *
*  following exceptions:                                                     *
*                                                                            *
*  If other files instantiate templates or use macros                        *
*  or inline functions from this file, or you compile this file and          *
*  link it with other files to produce an executable, this file does         *
*  not by itself cause the resulting executable to be covered by the         *
*  GNU Lesser General Public License. This exception does not however        *
*  invalidate any other reasons why the executable file might be             *
*  covered by the GNU Lesser General Public License.                         *
*                                                                            *
*  OpenFlipper is distributed in the hope that it will be useful,            *
*  but WITHOUT ANY WARRANTY; without even the implied warranty of            *
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             *
*  GNU Lesser General Public License for more details.                       *
*                                                                            *
*  You should have received a copy of the GNU LesserGeneral Public           *
*  License along with OpenFlipper. If not,                                   *
*  see <http://www.gnu.org/licenses/>.                                       *
*                                                                            *
\*===========================================================================*/


#pragma once

#include <QObject>

#include <OpenFlipper/common/Types.hh>
#include <OpenFlipper/BasePlugin/BaseInterface.hh>
#include <OpenFlipper/BasePlugin/FileInterface.hh>
#include <OpenFlipper/BasePlugin/LoadSaveInterface.hh>
#include <OpenFlipper/BasePlugin/LoggingInterface.hh>
#include <OpenFlipper/BasePlugin/ScriptInterface.hh>
#include <OpenFlipper/BasePlugin/TypeInterface.hh>
#include <OpenFlipper/BasePlugin/RPCInterface.hh>
#include <OpenFlipper/BasePlugin/AboutInfoInterface.hh>
#include <OpenFlipper/BasePlugin/ProcessInterface.hh>
#include <OpenFlipper/BasePlugin/TextureInterface.hh>

#include <ObjectTypes/PolyMesh/PolyMesh.hh>
#include <ObjectTypes/TriangleMesh/TriangleMesh.hh>

#include <assimp/Importer.hpp>
#include <assimp/Exporter.hpp>
#include <assimp/postprocess.h>
#include <assimp/scene.h>

#include<unordered_map>

#include "AssimpPluginCommon.hh"

#include <QComboBox>
#include <QCheckBox>

class AssimpOptionsWidget;

class AssimpPlugin : public QObject, BaseInterface, FileInterface, LoadSaveInterface,
    LoggingInterface, ScriptInterface, RPCInterface, AboutInfoInterface, ProcessInterface, TextureInterface
{
  Q_OBJECT
  Q_INTERFACES(FileInterface)
  Q_INTERFACES(LoadSaveInterface)
  Q_INTERFACES(LoggingInterface)
  Q_INTERFACES(BaseInterface)
  Q_INTERFACES(ScriptInterface)
  Q_INTERFACES(RPCInterface)
  Q_INTERFACES(AboutInfoInterface)
  Q_INTERFACES(ProcessInterface)
  Q_INTERFACES(TextureInterface)

  Q_PLUGIN_METADATA(IID "org.OpenFlipper.Plugins.Plugin-Assimp")


  signals:
    void openedFile( int _id );
    void addEmptyObject( DataType _type, int& _id);
    void load(QString _filename, DataType _type, int& _id);
    void save(int _id , QString _filename );
    void log(Logtype _type, QString _message);
    void log(QString _message);
    void updateView();
    void updatedObject(int _identifier, const UpdateType& _type);

    void deleteObject( int _id );

    //RPCInterface
    void pluginExists( QString _pluginName , bool& _exists  );

    //AboutInfoInterface
    void addAboutInfo(QString _text, QString _tabName );
    
    //texture interface
    void addTexture( QString _name , QImage _image , uint _dimension , int _id );
    void switchTexture( QString _textureName , int _id  );
    void setTextureMode(QString _textureName, QString _mode, int _id );
    
    
public:
  AssimpPlugin();

  QString name() { return (QString("AssimpPlugin")); }
  QString description( ) { return (QString(tr("Load/Save Files with the assimp library"))); }

  DataType supportedType();

  QString getSaveFilters();
  QString getLoadFilters();

  QWidget* saveOptionsWidget(QString /*_currentFilter*/);
  QWidget* loadOptionsWidget(QString /*_currentFilter*/);

public slots:

  /// Loads Object and converts it to a triangle mesh if possible
  int loadObject(QString _filename);

  /// Loads Object with given datatype
  int loadObject(QString _filename, DataType _type);

  bool saveObject(int _id, QString _filename);

  QString version() { return QString("1.0"); }

private slots:

  void fileOpened( int /*_id*/ ){}

  void noguiSupported( ) {}

  void initializePlugin();
  
//  void slotLoadDefault();
//  void slotSaveDefault();
  
  /// displays a message dialog with the given message, 
  /// and returns whether confirm or cancel was clicked
  /// returns true if confirm was clicked
  bool slotShowConfirmationDialog(const QString& _title, const QString & _message);
  
  /// slot gets called when a user clicks on the cancel button.
  void canceledJob(QString _jobId);
  
signals:
  bool showConfirmationDialog(const QString& _title, const QString & _message);
 // void addTexture(TextureNode* _node, const QImage& img);

private:

  /// converts an aiScene to a polymesh or trimesh
  int convertAiSceneToOpenMesh(const aiScene* _scene, QString _objectName);

  /// converts a polymesh or trimesh to an aiScene
  bool convertOpenMeshToAiScene(aiScene* _scene, BaseObjectData* _object);

  /// converts _mesh into _polyMesh
  void convertAiMeshToPolyMesh(PolyMesh* _polyMesh, aiMesh* _mesh);

  /// converts _mesh into _triMesh
  void convertAiMeshToTriMesh(TriMesh* _triMesh, aiMesh* _mesh);

  /// converts a polymesh to an aiMesh
  bool convertPolyMeshToAiMesh(PolyMesh* _polyMesh, aiMesh* _mesh);

  /// converts a trimesh to an aiMesh
  bool convertTriMeshToAiMesh(TriMesh* _triMesh, aiMesh* _mesh);

  /// add a vertex from _mesh to _polyMesh and stores the index to handle mapping
  //void mapVertices(PolyMesh* _polyMesh, aiMesh* _mesh);

  /// add a vertex from _mesh to _trimesh and stores the index to handle mapping
  //void mapVertices(TriMesh* _triMesh, aiMesh* _mesh);
  
  template<typename Mesh>
  void mapVertices(Mesh* _triMesh, aiMesh* _mesh);
  
  /// returns which postprocessing steps are enabled for the loader
  //unsigned int processSteps(const std::unordered_map<QCheckBox*, unsigned int>& _options);
  
  /// loads a material from the given aiScene to a given MaterialNode
  void loadMaterial(const aiScene* _scene, MaterialNode* _matNode, unsigned int _materialIndex);
  
  /// loads all embedded textures from a scene
  void loadTexturesFromScene(const aiScene* _scene);
  
  /// load all textures of a specific type from a material
  /// this also loads textures which are not embedded, but specified by a file path
  std::vector<std::pair<std::string,QImage>> loadTexturesFromMaterial(const aiMaterial* _mat, aiTextureType _textureType);
  
  /// loads all lights from a scene
  void loadLightsFromScene(const aiScene* _scene);
  
  /// loads all cameras from a scene
  void loadCamerasFromScene(const aiScene* _scene);
  
  /// loads all animations from a scene
  void loadAnimationsFromScene(const aiScene* _scene);
  
  /// loads all meshes from a scene  
  void loadMeshesFromScene(const aiScene* _scene, std::vector<int> & _outIds, QString& _objectName);
  

private:
  //Option Widgets
  //QWidget      *loadOptions_, *saveOptions_;
  AssimpOptionsWidget      *loadOptions_, *saveOptions_;
  
  QComboBox    *triMeshHandling_;
  
  QCheckBox    * loadVertexColor_ , * loadNormals_ , * loadTexCoords_ , * loadTextures_ ,
               * loadLights_      , * loadCameras_ , * loadAnimations_                  ;
  
  QPushButton* loadDefaultButton_;
  
  LoaderOptions options_;
  
  QCheckBox* load_assimp_process_calcTangentSpace        ;
  QCheckBox* load_assimp_process_joinIdenticalVertices   ;
  QCheckBox* load_assimp_process_makeLeftHanded          ;
  QCheckBox* load_assimp_process_triangulate             ;
  QCheckBox* load_assimp_process_removeComponent         ;
  QCheckBox* load_assimp_process_genNormals              ;
  QCheckBox* load_assimp_process_genSmoothNormals        ;
  QCheckBox* load_assimp_process_splitLargeMeshes        ;
  QCheckBox* load_assimp_process_preTransformVertices    ;
  QCheckBox* load_assimp_process_limitBoneWeights        ;
  QCheckBox* load_assimp_process_validateDataStructure   ;
  QCheckBox* load_assimp_process_improveCacheLocality    ;
  QCheckBox* load_assimp_process_removeRedundantMaterials;
  QCheckBox* load_assimp_process_fixInfacingNormals      ;
  QCheckBox* load_assimp_process_sortByPType             ;
  QCheckBox* load_assimp_process_findDegenerates         ;
  QCheckBox* load_assimp_process_findInvalidData         ;
  QCheckBox* load_assimp_process_genUVCoords             ;
  QCheckBox* load_assimp_process_transformUVCoords       ;
  QCheckBox* load_assimp_process_findInstances           ;
  QCheckBox* load_assimp_process_optimizeMeshes          ;
  QCheckBox* load_assimp_process_optimizeGraph           ;
  QCheckBox* load_assimp_process_flipUVs                 ;
  QCheckBox* load_assimp_process_flipWindingOrder        ;
  QCheckBox* load_assimp_process_splitByBoneCount        ;
  QCheckBox* load_assimp_process_debone                  ;
  
  std::unordered_map<QCheckBox*, unsigned int> loadOptions;
  unsigned int  processSteps;

  QCheckBox*   saveVertexColor_;
  QCheckBox*   saveFaceColor_;
  QCheckBox*   saveAlpha_;
  QCheckBox*   saveNormals_;
  QCheckBox*   saveTexCoords_;
  QCheckBox*   saveTextures_;
  QPushButton* saveDefaultButton_;
  
  QCheckBox* save_assimp_process_calcTangentSpace        ;
  QCheckBox* save_assimp_process_joinIdenticalVertices   ;
  QCheckBox* save_assimp_process_makeLeftHanded          ;
  QCheckBox* save_assimp_process_triangulate             ;
  QCheckBox* save_assimp_process_removeComponent         ;
  QCheckBox* save_assimp_process_genNormals              ;
  QCheckBox* save_assimp_process_genSmoothNormals        ;
  QCheckBox* save_assimp_process_splitLargeMeshes        ;
  QCheckBox* save_assimp_process_preTransformVertices    ;
  QCheckBox* save_assimp_process_limitBoneWeights        ;
  QCheckBox* save_assimp_process_validateDataStructure   ;
  QCheckBox* save_assimp_process_improveCacheLocality    ;
  QCheckBox* save_assimp_process_removeRedundantMaterials;
  QCheckBox* save_assimp_process_fixInfacingNormals      ;
  QCheckBox* save_assimp_process_sortByPType             ;
  QCheckBox* save_assimp_process_findDegenerates         ;
  QCheckBox* save_assimp_process_findInvalidData         ;
  QCheckBox* save_assimp_process_genUVCoords             ;
  QCheckBox* save_assimp_process_transformUVCoords       ;
  QCheckBox* save_assimp_process_findInstances           ;
  QCheckBox* save_assimp_process_optimizeMeshes          ;
  QCheckBox* save_assimp_process_optimizeGraph           ;
  QCheckBox* save_assimp_process_flipUVs                 ;
  QCheckBox* save_assimp_process_flipWindingOrder        ;
  QCheckBox* save_assimp_process_splitByBoneCount        ;
  QCheckBox* save_assimp_process_debone                  ;
  
  std::unordered_map<QCheckBox*, unsigned int> saveOptions;
  
  DataType type_;

  /// maps indices of vertices in an aiMesh to OpenMesh VertexHandles
  std::map<unsigned int, OpenMesh::VertexHandle> vertexHandles_;
  
  /// cancels loading
  bool cancel_;
  
  /// a list of images, that was loaded by material type
  std::unordered_map<unsigned int, std::vector<std::pair<std::string, QImage>>> loadedImages_;
  std::vector<std::pair<std::string, QImage>> embeddedTextures_;
};

#include "AssimpPluginT_impl.hh"

