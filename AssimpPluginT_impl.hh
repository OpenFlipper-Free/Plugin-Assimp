/*===========================================================================*\
 *                                                                           *
 *                             OpenFlipper                                   *
 *           Copyright (c) 2001-2015, RWTH-Aachen University                 *
 *           Department of Computer Graphics and Multimedia                  *
 *                          All rights reserved.                             *
 *                            www.openflipper.org                            *
 *                                                                           *
 *---------------------------------------------------------------------------*
 * This file is part of OpenFlipper.                                         *
 *---------------------------------------------------------------------------*
 *                                                                           *
 * Redistribution and use in source and binary forms, with or without        *
 * modification, are permitted provided that the following conditions        *
 * are met:                                                                  *
 *                                                                           *
 * 1. Redistributions of source code must retain the above copyright notice, *
 *    this list of conditions and the following disclaimer.                  *
 *                                                                           *
 * 2. Redistributions in binary form must reproduce the above copyright      *
 *    notice, this list of conditions and the following disclaimer in the    *
 *    documentation and/or other materials provided with the distribution.   *
 *                                                                           *
 * 3. Neither the name of the copyright holder nor the names of its          *
 *    contributors may be used to endorse or promote products derived from   *
 *    this software without specific prior written permission.               *
 *                                                                           *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS       *
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED *
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A           *
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER *
 * OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,  *
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,       *
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR        *
 * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF    *
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING      *
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS        *
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.              *
 *                                                                           *
\*===========================================================================*/

#pragma once

template <typename Mesh>
void AssimpPlugin::mapVertices(Mesh *_polyMesh, aiMesh *_mesh) {
  vertexHandles_.clear();
    std::cout<<"mesh has "<<_mesh->GetNumColorChannels()<<" color channels"<<std::endl;
    bool tc1requested = false;
    bool tc2requested = false;
    bool tc3requested = false;
    //
  for (unsigned int i = 0; i < _mesh->mNumVertices; ++i) {
    vertexHandles_[i] = _polyMesh->add_vertex(ACG::Vec3d(_mesh->mVertices[i].x, _mesh->mVertices[i].y, _mesh->mVertices[i].z));

    //load vertexColors
    for(unsigned int j = 0 ; j < _mesh->GetNumColorChannels(); ++j)
    {
      if(options_.loadVertexColorOption_ && _mesh->HasVertexColors(j))
        _polyMesh->set_color(vertexHandles_[i], PolyMesh::Color(_mesh->mColors[j][i].r, _mesh->mColors[j][i].g, _mesh->mColors[j][i].b, _mesh->mColors[j][i].a));
    }
    
    //load texCoords 
    for(unsigned int j = 0 ; j < _mesh->GetNumUVChannels(); ++j)
    {
      if(options_.loadTexCoordsOption_ && _mesh->HasTextureCoords(j))
      {
        if(_mesh->mNumUVComponents[j] == 1)
        {
          if(!tc1requested)
          {
            _polyMesh->request_vertex_texcoords1D();
            tc1requested = true;
          }
          _polyMesh->set_texcoord1D(vertexHandles_[i], PolyMesh::TexCoord1D(_mesh->mTextureCoords[j][i].x));
        }
        if(_mesh->mNumUVComponents[j] == 2)
        {
          if(!tc2requested)
          {
          _polyMesh->request_vertex_texcoords2D();
          tc2requested = true;
          }
          _polyMesh->set_texcoord2D(vertexHandles_[i], PolyMesh::TexCoord2D(_mesh->mTextureCoords[j][i].x, _mesh->mTextureCoords[j][i].y));
        }
        if(_mesh->mNumUVComponents[j] == 3)
        {
          if(!tc3requested)
          {
            _polyMesh->request_vertex_texcoords3D();
            tc3requested = true;
          }
          _polyMesh->set_texcoord3D(vertexHandles_[i], PolyMesh::TexCoord3D(_mesh->mTextureCoords[j][i].x, _mesh->mTextureCoords[j][i].y, _mesh->mTextureCoords[j][i].z));
        }
      }
    }
  }
}
