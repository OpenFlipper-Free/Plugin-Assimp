/*===========================================================================*\
 *                                                                           *
 *                             OpenFlipper                                   *
 *           Copyright (c) 2001-2015, RWTH-Aachen University                 *
 *           Department of Computer Graphics and Multimedia                  *
 *                          All rights reserved.                             *
 *                            www.openflipper.org                            *
 *                                                                           *
 *---------------------------------------------------------------------------*
 * This file is part of OpenFlipper.                                         *
 *---------------------------------------------------------------------------*
 *                                                                           *
 * Redistribution and use in source and binary forms, with or without        *
 * modification, are permitted provided that the following conditions        *
 * are met:                                                                  *
 *                                                                           *
 * 1. Redistributions of source code must retain the above copyright notice, *
 *    this list of conditions and the following disclaimer.                  *
 *                                                                           *
 * 2. Redistributions in binary form must reproduce the above copyright      *
 *    notice, this list of conditions and the following disclaimer in the    *
 *    documentation and/or other materials provided with the distribution.   *
 *                                                                           *
 * 3. Neither the name of the copyright holder nor the names of its          *
 *    contributors may be used to endorse or promote products derived from   *
 *    this software without specific prior written permission.               *
 *                                                                           *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS       *
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED *
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A           *
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER *
 * OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,  *
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,       *
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR        *
 * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF    *
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING      *
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS        *
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.              *
 *                                                                           *
\*===========================================================================*/

#include "AssimpLoadOptionsWidget.hh"
#include <assimp/postprocess.h>
#include <OpenFlipper/common/GlobalOptions.hh>
#include "../AssimpPluginCommon.hh"

AssimpLoadOptionsWidget::AssimpLoadOptionsWidget(LoaderOptions & loader, QWidget * parent )
  : AssimpOptionsWidget ("Load", loader, parent)
{ 
  // generic configuration
  triMeshHandling_ = new QComboBox();
  triMeshHandling_->addItem( tr("Detect correct mesh type") );
  triMeshHandling_->addItem( tr("Ask") );
  triMeshHandling_->addItem( tr("Open as PolyMesh") );
  triMeshHandling_->addItem( tr("Open as TriangleMesh") );      

  loadVertexColor_ = new QCheckBox("Load Vertex Colors");      
  loadNormals_     = new QCheckBox("Load Vertex Normals");      
  loadTexCoords_   = new QCheckBox("Load Vertex Texture Coordinates");      
  loadTextures_    = new QCheckBox("Load Textures");
  loadLights_      = new QCheckBox("Load Lights"); 
  loadCameras_     = new QCheckBox("Load Cameras");
  loadAnimations_  = new QCheckBox("Load Animations");
  
#ifndef ENABLE_LIGHT_SUPPORT
  loadLights_->setEnabled(false);
#endif
#ifndef ENABLE_CAMERA_SUPPORT
  loadCameras_->setEnabled(false);
#endif
#ifndef ENABLE_SKELETON_SUPPORT
  loadAnimations_->setEnabled(false);
#endif

  connect(loadVertexColor_, &QCheckBox::toggled, [&](bool _toggled){loader.loadVertexColorOption_ = _toggled ;});
  connect(loadNormals_    , &QCheckBox::toggled, [&](bool _toggled){loader.loadNormalsOption_     = _toggled ;});
  connect(loadTexCoords_  , &QCheckBox::toggled, [&](bool _toggled){loader.loadTexCoordsOption_   = _toggled ;});
  connect(loadTextures_   , &QCheckBox::toggled, [&](bool _toggled){loader.loadTexturesOption_    = _toggled ;});
  connect(loadLights_     , &QCheckBox::toggled, [&](bool _toggled){loader.loadLightsOption_      = _toggled ;});
  connect(loadCameras_    , &QCheckBox::toggled, [&](bool _toggled){loader.loadCamerasOption_     = _toggled ;});
  connect(loadAnimations_ , &QCheckBox::toggled, [&](bool _toggled){loader.loadAnimationsOption_  = _toggled ;}); 
  
  connect(triMeshHandling_ , static_cast<void (QComboBox::*)(int)>(&QComboBox::currentIndexChanged), [&loader](unsigned int _index){loader.triMeshHandling_ = _index;});
  
  QList<QWidget*> generalOptions = {triMeshHandling_, loadVertexColor_, loadNormals_, loadTexCoords_,
                                    loadTextures_, loadLights_, loadCameras_, loadAnimations_};
  

  QTreeWidgetItem* general = new QTreeWidgetItem();
  general->setText(0,"General");
  tree->invisibleRootItem()->insertChild(0,general);

  
  for(QList<QWidget*>::iterator it = generalOptions.begin() ; it != generalOptions.end(); ++it)
  {
    QTreeWidgetItem* treeWidget = new QTreeWidgetItem();
    general->addChild(treeWidget);
    tree->setItemWidget(treeWidget,0,*it);
  }  
  
  connect(makeDefaultButton_, SIGNAL(clicked()), this, SLOT(slotMakeDefault()));

  triMeshHandling_->setCurrentIndex(OpenFlipperSettings().value("Assimp/Load/TriMeshHandling",TYPEAUTODETECT).toInt() );

  loadVertexColor_->setChecked( OpenFlipperSettings().value("Assimp/Load/FaceColor",true).toBool()  );
  loadNormals_->setChecked  ( OpenFlipperSettings().value("Assimp/Load/Normals"  ,true).toBool()  );
  loadTexCoords_->setChecked( OpenFlipperSettings().value("Assimp/Load/TexCoords",true).toBool()  );
  loadTextures_->setChecked ( OpenFlipperSettings().value("Assimp/Load/Textures" ,true).toBool()  );
}

void AssimpLoadOptionsWidget::slotMakeDefault()
{
  OpenFlipperSettings().setValue( "Assimp/Load/FaceColor",   loadVertexColor_->isChecked()  );
  OpenFlipperSettings().setValue( "Assimp/Load/Normals",     loadNormals_->isChecked()  );
  OpenFlipperSettings().setValue( "Assimp/Load/TexCoords",   loadTexCoords_->isChecked()  );
  OpenFlipperSettings().setValue( "Assimp/Load/Textures",    loadTextures_->isChecked()  );
  OpenFlipperSettings().setValue( "Assimp/Load/TriMeshHandling", triMeshHandling_->currentIndex() );
  
  OpenFlipperSettings().setValue( "Core/File/UseLoadDefaults", true );
}
