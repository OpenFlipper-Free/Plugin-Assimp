/*===========================================================================*\
 *                                                                           *
 *                             OpenFlipper                                   *
 *           Copyright (c) 2001-2015, RWTH-Aachen University                 *
 *           Department of Computer Graphics and Multimedia                  *
 *                          All rights reserved.                             *
 *                            www.openflipper.org                            *
 *                                                                           *
 *---------------------------------------------------------------------------*
 * This file is part of OpenFlipper.                                         *
 *---------------------------------------------------------------------------*
 *                                                                           *
 * Redistribution and use in source and binary forms, with or without        *
 * modification, are permitted provided that the following conditions        *
 * are met:                                                                  *
 *                                                                           *
 * 1. Redistributions of source code must retain the above copyright notice, *
 *    this list of conditions and the following disclaimer.                  *
 *                                                                           *
 * 2. Redistributions in binary form must reproduce the above copyright      *
 *    notice, this list of conditions and the following disclaimer in the    *
 *    documentation and/or other materials provided with the distribution.   *
 *                                                                           *
 * 3. Neither the name of the copyright holder nor the names of its          *
 *    contributors may be used to endorse or promote products derived from   *
 *    this software without specific prior written permission.               *
 *                                                                           *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS       *
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED *
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A           *
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER *
 * OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,  *
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,       *
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR        *
 * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF    *
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING      *
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS        *
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.              *
 *                                                                           *
\*===========================================================================*/

#pragma once

#include "ui_AssimpOptionsWidget.h"

#include <QCheckBox>

#include <unordered_map>

struct LoaderOptions;

class AssimpOptionsWidget : public QWidget, public Ui::AssimpOptionsWidget
{
  Q_OBJECT

public:
  AssimpOptionsWidget(const QString & _settingPrefix, LoaderOptions& loader, QWidget * parent = 0);
  
  QString settingPrefix;
  
  QCheckBox* option_assimp_process_calcTangentSpace        ;
  QCheckBox* option_assimp_process_joinIdenticalVertices   ;
  QCheckBox* option_assimp_process_makeLeftHanded          ;
  QCheckBox* option_assimp_process_triangulate             ;
  QCheckBox* option_assimp_process_removeComponent         ;
  QCheckBox* option_assimp_process_genNormals              ;
  QCheckBox* option_assimp_process_genSmoothNormals        ;
  QCheckBox* option_assimp_process_splitLargeMeshes        ;
  QCheckBox* option_assimp_process_preTransformVertices    ;
  QCheckBox* option_assimp_process_limitBoneWeights        ;
  QCheckBox* option_assimp_process_validateDataStructure   ;
  QCheckBox* option_assimp_process_improveCacheLocality    ;
  QCheckBox* option_assimp_process_removeRedundantMaterials;
  QCheckBox* option_assimp_process_fixInfacingNormals      ;
  QCheckBox* option_assimp_process_sortByPType             ;
  QCheckBox* option_assimp_process_findDegenerates         ;
  QCheckBox* option_assimp_process_findInvalidData         ;
  QCheckBox* option_assimp_process_genUVCoords             ;
  QCheckBox* option_assimp_process_transformUVCoords       ;
  QCheckBox* option_assimp_process_findInstances           ;
  QCheckBox* option_assimp_process_optimizeMeshes          ;
  QCheckBox* option_assimp_process_optimizeGraph           ;
  QCheckBox* option_assimp_process_flipUVs                 ;
  QCheckBox* option_assimp_process_flipWindingOrder        ;
  QCheckBox* option_assimp_process_splitByBoneCount        ;
  QCheckBox* option_assimp_process_debone                  ;
  
  std::unordered_map<QCheckBox*, unsigned int> loadOptions;
  
  unsigned int processSteps();
  
private slots:
  
  void slotMakeDefault();
//  void slotSaveDefault();
  
};
