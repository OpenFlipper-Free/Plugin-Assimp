/*===========================================================================*\
 *                                                                           *
 *                             OpenFlipper                                   *
 *           Copyright (c) 2001-2015, RWTH-Aachen University                 *
 *           Department of Computer Graphics and Multimedia                  *
 *                          All rights reserved.                             *
 *                            www.openflipper.org                            *
 *                                                                           *
 *---------------------------------------------------------------------------*
 * This file is part of OpenFlipper.                                         *
 *---------------------------------------------------------------------------*
 *                                                                           *
 * Redistribution and use in source and binary forms, with or without        *
 * modification, are permitted provided that the following conditions        *
 * are met:                                                                  *
 *                                                                           *
 * 1. Redistributions of source code must retain the above copyright notice, *
 *    this list of conditions and the following disclaimer.                  *
 *                                                                           *
 * 2. Redistributions in binary form must reproduce the above copyright      *
 *    notice, this list of conditions and the following disclaimer in the    *
 *    documentation and/or other materials provided with the distribution.   *
 *                                                                           *
 * 3. Neither the name of the copyright holder nor the names of its          *
 *    contributors may be used to endorse or promote products derived from   *
 *    this software without specific prior written permission.               *
 *                                                                           *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS       *
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED *
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A           *
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER *
 * OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,  *
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,       *
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR        *
 * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF    *
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING      *
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS        *
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.              *
 *                                                                           *
\*===========================================================================*/

#include "AssimpSaveOptionsWidget.hh"
#include <assimp/postprocess.h>
#include <OpenFlipper/common/GlobalOptions.hh>
#include "../AssimpPluginCommon.hh"

AssimpSaveOptionsWidget::AssimpSaveOptionsWidget(LoaderOptions & loader, QWidget * parent )
  : AssimpOptionsWidget ("Save", loader, parent)
{ 
  // generic configuration    
  saveVertexColor_ = new QCheckBox("Save Vertex Colors");      
  saveNormals_     = new QCheckBox("Save Vertex Normals");      
  saveTexCoords_   = new QCheckBox("Save Vertex Texture Coordinates");      
  saveTextures_    = new QCheckBox("Save Textures");
  saveLights_      = new QCheckBox("Save Lights"); 
  saveCameras_     = new QCheckBox("Save Cameras");
  saveAnimations_  = new QCheckBox("Save Animations");
  
#ifndef ENABLE_LIGHT_SUPPORT
  saveLights_->setEnabled(false);
#endif
#ifndef ENABLE_CAMERA_SUPPORT
  saveCameras_->setEnabled(false);
#endif
#ifndef ENABLE_SKELETON_SUPPORT
  saveAnimations_->setEnabled(false);
#endif

  connect(saveVertexColor_, &QCheckBox::toggled, [&](bool _toggled){loader.loadVertexColorOption_ = _toggled ;});
  connect(saveNormals_    , &QCheckBox::toggled, [&](bool _toggled){loader.loadNormalsOption_     = _toggled ;});
  connect(saveTexCoords_  , &QCheckBox::toggled, [&](bool _toggled){loader.loadTexCoordsOption_   = _toggled ;});
  connect(saveTextures_   , &QCheckBox::toggled, [&](bool _toggled){loader.loadTexturesOption_    = _toggled ;});
  connect(saveLights_     , &QCheckBox::toggled, [&](bool _toggled){loader.loadLightsOption_      = _toggled ;});
  connect(saveCameras_    , &QCheckBox::toggled, [&](bool _toggled){loader.loadCamerasOption_     = _toggled ;});
  connect(saveAnimations_ , &QCheckBox::toggled, [&](bool _toggled){loader.loadAnimationsOption_  = _toggled ;});   
  
  QList<QWidget*> generalOptions = {saveVertexColor_, saveNormals_, saveTexCoords_,
                                    saveTextures_   , saveLights_ , saveCameras_  , saveAnimations_};
  

  QTreeWidgetItem* general = new QTreeWidgetItem();
  general->setText(0,"General");
  tree->invisibleRootItem()->insertChild(0,general);

  
  for(QList<QWidget*>::iterator it = generalOptions.begin() ; it != generalOptions.end(); ++it)
  {
    QTreeWidgetItem* treeWidget = new QTreeWidgetItem();
    general->addChild(treeWidget);
    tree->setItemWidget(treeWidget,0,*it);
  }  
  
  connect(makeDefaultButton_, SIGNAL(clicked()), this, SLOT(slotMakeDefault()));

  saveVertexColor_->setChecked( OpenFlipperSettings().value("Assimp/Save/FaceColor",true).toBool()  );
  saveNormals_->setChecked    ( OpenFlipperSettings().value("Assimp/Save/Normals"  ,true).toBool()  );
  saveTexCoords_->setChecked  ( OpenFlipperSettings().value("Assimp/Save/TexCoords",true).toBool()  );
  saveTextures_->setChecked   ( OpenFlipperSettings().value("Assimp/Save/Textures" ,true).toBool()  );
}

void AssimpSaveOptionsWidget::slotMakeDefault()
{
  OpenFlipperSettings().setValue( "Assimp/Save/FaceColor",   saveVertexColor_->isChecked()  );
  OpenFlipperSettings().setValue( "Assimp/Save/Normals",     saveNormals_->isChecked()  );
  OpenFlipperSettings().setValue( "Assimp/Save/TexCoords",   saveTexCoords_->isChecked()  );
  OpenFlipperSettings().setValue( "Assimp/Save/Textures",    saveTextures_->isChecked()  );
  
  OpenFlipperSettings().setValue( "Core/File/UseLoadDefaults", true );
}

