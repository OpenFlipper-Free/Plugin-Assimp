/*===========================================================================*\
 *                                                                           *
 *                             OpenFlipper                                   *
 *           Copyright (c) 2001-2015, RWTH-Aachen University                 *
 *           Department of Computer Graphics and Multimedia                  *
 *                          All rights reserved.                             *
 *                            www.openflipper.org                            *
 *                                                                           *
 *---------------------------------------------------------------------------*
 * This file is part of OpenFlipper.                                         *
 *---------------------------------------------------------------------------*
 *                                                                           *
 * Redistribution and use in source and binary forms, with or without        *
 * modification, are permitted provided that the following conditions        *
 * are met:                                                                  *
 *                                                                           *
 * 1. Redistributions of source code must retain the above copyright notice, *
 *    this list of conditions and the following disclaimer.                  *
 *                                                                           *
 * 2. Redistributions in binary form must reproduce the above copyright      *
 *    notice, this list of conditions and the following disclaimer in the    *
 *    documentation and/or other materials provided with the distribution.   *
 *                                                                           *
 * 3. Neither the name of the copyright holder nor the names of its          *
 *    contributors may be used to endorse or promote products derived from   *
 *    this software without specific prior written permission.               *
 *                                                                           *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS       *
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED *
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A           *
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER *
 * OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,  *
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,       *
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR        *
 * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF    *
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING      *
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS        *
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.              *
 *                                                                           *
\*===========================================================================*/

#include "AssimpOptionsWidget.hh"
#include "ui_AssimpOptionsWidget.h"
#include <assimp/postprocess.h>
#include <OpenFlipper/common/GlobalOptions.hh>
#include "../AssimpPluginCommon.hh"

AssimpOptionsWidget::AssimpOptionsWidget(const QString & _settingPrefix, LoaderOptions & loader, QWidget * parent )
  : QWidget(parent),
    settingPrefix(_settingPrefix)
{
  setupUi(this);
  
  // setup the treeviewWidget and add an option called advanced for assimp settings
  tree->setHeaderLabel("Configuration:");
  QTreeWidgetItem* advanced = new QTreeWidgetItem();
  advanced->setText(0,"Advanced");
  tree->invisibleRootItem()->addChild(advanced);
  
  //create checkboxes for assimp postprocessing options
  option_assimp_process_calcTangentSpace         = new QCheckBox("assimp_process_calcTangentSpace        ");
  option_assimp_process_joinIdenticalVertices    = new QCheckBox("assimp_process_joinIdenticalVertices   ");
  option_assimp_process_makeLeftHanded           = new QCheckBox("assimp_process_makeLeftHanded          ");
  option_assimp_process_triangulate              = new QCheckBox("assimp_process_triangulate             ");
  option_assimp_process_removeComponent          = new QCheckBox("assimp_process_removeComponent         ");
  option_assimp_process_genNormals               = new QCheckBox("assimp_process_genNormals              ");
  option_assimp_process_genSmoothNormals         = new QCheckBox("assimp_process_genSmoothNormals        ");
  option_assimp_process_splitLargeMeshes         = new QCheckBox("assimp_process_splitLargeMeshes        ");
  option_assimp_process_preTransformVertices     = new QCheckBox("assimp_process_preTransformVertices    ");
  option_assimp_process_limitBoneWeights         = new QCheckBox("assimp_process_limitBoneWeights        ");
  option_assimp_process_validateDataStructure    = new QCheckBox("assimp_process_validateDataStructure   ");
  option_assimp_process_improveCacheLocality     = new QCheckBox("assimp_process_improveCacheLocality    ");
  option_assimp_process_removeRedundantMaterials = new QCheckBox("assimp_process_removeRedundantMaterials");
  option_assimp_process_fixInfacingNormals       = new QCheckBox("assimp_process_fixInfacingNormals      ");
  option_assimp_process_sortByPType              = new QCheckBox("assimp_process_sortByPType             ");
  option_assimp_process_findDegenerates          = new QCheckBox("assimp_process_findDegenerates         ");
  option_assimp_process_findInvalidData          = new QCheckBox("assimp_process_findInvalidData         ");
  option_assimp_process_genUVCoords              = new QCheckBox("assimp_process_genUVCoords             ");
  option_assimp_process_transformUVCoords        = new QCheckBox("assimp_process_transformUVCoords       ");
  option_assimp_process_findInstances            = new QCheckBox("assimp_process_findInstances           ");
  option_assimp_process_optimizeMeshes           = new QCheckBox("assimp_process_optimizeMeshes          ");
  option_assimp_process_optimizeGraph            = new QCheckBox("assimp_process_optimizeGraph           ");
  option_assimp_process_flipUVs                  = new QCheckBox("assimp_process_flipUVs                 ");
  option_assimp_process_flipWindingOrder         = new QCheckBox("assimp_process_flipWindingOrder        ");
  option_assimp_process_splitByBoneCount         = new QCheckBox("assimp_process_splitByBoneCount        ");
  option_assimp_process_debone                   = new QCheckBox("assimp_process_debone                  ");

  //store a mapping for each checkbox to a corresponding postprocess setting
  loadOptions= {{option_assimp_process_calcTangentSpace        , aiProcess_CalcTangentSpace        },
                {option_assimp_process_joinIdenticalVertices   , aiProcess_JoinIdenticalVertices   },
                {option_assimp_process_makeLeftHanded          , aiProcess_MakeLeftHanded          },
                {option_assimp_process_triangulate             , aiProcess_Triangulate             },
                {option_assimp_process_removeComponent         , aiProcess_RemoveComponent         },
                {option_assimp_process_genNormals              , aiProcess_GenNormals              },
                {option_assimp_process_genSmoothNormals        , aiProcess_GenSmoothNormals        },
                {option_assimp_process_splitLargeMeshes        , aiProcess_SplitLargeMeshes        },
                {option_assimp_process_preTransformVertices    , aiProcess_PreTransformVertices    },
                {option_assimp_process_limitBoneWeights        , aiProcess_LimitBoneWeights        },
                {option_assimp_process_validateDataStructure   , aiProcess_ValidateDataStructure   },
                {option_assimp_process_improveCacheLocality    , aiProcess_ImproveCacheLocality    },
                {option_assimp_process_removeRedundantMaterials, aiProcess_RemoveRedundantMaterials},
                {option_assimp_process_fixInfacingNormals      , aiProcess_FixInfacingNormals      },
                {option_assimp_process_sortByPType             , aiProcess_SortByPType             },
                {option_assimp_process_findDegenerates         , aiProcess_FindDegenerates         },
                {option_assimp_process_findInvalidData         , aiProcess_FindInvalidData         },
                {option_assimp_process_genUVCoords             , aiProcess_GenUVCoords             },
                {option_assimp_process_transformUVCoords       , aiProcess_TransformUVCoords       },
                {option_assimp_process_findInstances           , aiProcess_FindInstances           },
                {option_assimp_process_optimizeMeshes          , aiProcess_OptimizeMeshes          },
                {option_assimp_process_optimizeGraph           , aiProcess_OptimizeGraph           },
                {option_assimp_process_flipUVs                 , aiProcess_FlipUVs                 },
                {option_assimp_process_flipWindingOrder        , aiProcess_FlipWindingOrder        },
                {option_assimp_process_splitByBoneCount        , aiProcess_SplitByBoneCount        },
                {option_assimp_process_debone                  , aiProcess_Debone                  }};
  
  //set the Checkboxes as widgets for the treeview items
  for(auto it = loadOptions.begin() ; it != loadOptions.end(); ++it)
  {
    QTreeWidgetItem* treeWidget = new QTreeWidgetItem();
    advanced->addChild(treeWidget);
    tree->setItemWidget(treeWidget,0,it->first);
  }
  
  //connect the make default button
  connect(makeDefaultButton_, SIGNAL(clicked()), this, SLOT(slotMakeDefault()));
  
  //initialize checkboxes with stored settings
  option_assimp_process_calcTangentSpace         ->setChecked(OpenFlipperSettings().value( "Assimp/"+settingPrefix+"/process/calcTangentSpace"        , false).toBool());
  option_assimp_process_joinIdenticalVertices    ->setChecked(OpenFlipperSettings().value( "Assimp/"+settingPrefix+"/process/joinIdenticalVertices"   , true ).toBool());
  option_assimp_process_makeLeftHanded           ->setChecked(OpenFlipperSettings().value( "Assimp/"+settingPrefix+"/process/makeLeftHanded"          , false).toBool());
  option_assimp_process_triangulate              ->setChecked(OpenFlipperSettings().value( "Assimp/"+settingPrefix+"/process/triangulate"             , false).toBool());
  option_assimp_process_removeComponent          ->setChecked(OpenFlipperSettings().value( "Assimp/"+settingPrefix+"/process/removeComponent"         , false).toBool());
  option_assimp_process_genNormals               ->setChecked(OpenFlipperSettings().value( "Assimp/"+settingPrefix+"/process/genNormals"              , false).toBool());
  option_assimp_process_genSmoothNormals         ->setChecked(OpenFlipperSettings().value( "Assimp/"+settingPrefix+"/process/genSmoothNormals"        , false).toBool());
  option_assimp_process_splitLargeMeshes         ->setChecked(OpenFlipperSettings().value( "Assimp/"+settingPrefix+"/process/splitLargeMeshes"        , false).toBool());
  option_assimp_process_preTransformVertices     ->setChecked(OpenFlipperSettings().value( "Assimp/"+settingPrefix+"/process/preTransformVertices"    , false).toBool());
  option_assimp_process_limitBoneWeights         ->setChecked(OpenFlipperSettings().value( "Assimp/"+settingPrefix+"/process/limitBoneWeights"        , false).toBool());
  option_assimp_process_validateDataStructure    ->setChecked(OpenFlipperSettings().value( "Assimp/"+settingPrefix+"/process/validateDataStructure"   , false).toBool());
  option_assimp_process_improveCacheLocality     ->setChecked(OpenFlipperSettings().value( "Assimp/"+settingPrefix+"/process/improveCacheLocality"    , false).toBool());
  option_assimp_process_removeRedundantMaterials ->setChecked(OpenFlipperSettings().value( "Assimp/"+settingPrefix+"/process/removeRedundantMaterials", false).toBool());
  option_assimp_process_fixInfacingNormals       ->setChecked(OpenFlipperSettings().value( "Assimp/"+settingPrefix+"/process/fixInfacingNormals"      , false).toBool());
  option_assimp_process_sortByPType              ->setChecked(OpenFlipperSettings().value( "Assimp/"+settingPrefix+"/process/sortByPType"             , false).toBool());
  option_assimp_process_findDegenerates          ->setChecked(OpenFlipperSettings().value( "Assimp/"+settingPrefix+"/process/findDegenerates"         , true ).toBool());
  option_assimp_process_findInvalidData          ->setChecked(OpenFlipperSettings().value( "Assimp/"+settingPrefix+"/process/findInvalidData"         , false).toBool());
  option_assimp_process_genUVCoords              ->setChecked(OpenFlipperSettings().value( "Assimp/"+settingPrefix+"/process/genUVCoords"             , false).toBool());
  option_assimp_process_transformUVCoords        ->setChecked(OpenFlipperSettings().value( "Assimp/"+settingPrefix+"/process/transformUVCoords"       , false).toBool());
  option_assimp_process_findInstances            ->setChecked(OpenFlipperSettings().value( "Assimp/"+settingPrefix+"/process/findInstances"           , false).toBool());
  option_assimp_process_optimizeMeshes           ->setChecked(OpenFlipperSettings().value( "Assimp/"+settingPrefix+"/process/optimizeMeshes"          , false).toBool());
  option_assimp_process_optimizeGraph            ->setChecked(OpenFlipperSettings().value( "Assimp/"+settingPrefix+"/process/optimizeGraph"           , false).toBool());
  option_assimp_process_flipUVs                  ->setChecked(OpenFlipperSettings().value( "Assimp/"+settingPrefix+"/process/flipUVs"                 , false).toBool());
  option_assimp_process_flipWindingOrder         ->setChecked(OpenFlipperSettings().value( "Assimp/"+settingPrefix+"/process/flipWindingOrder"        , false).toBool());
  option_assimp_process_splitByBoneCount         ->setChecked(OpenFlipperSettings().value( "Assimp/"+settingPrefix+"/process/splitByBoneCount"        , false).toBool());
  option_assimp_process_debone                   ->setChecked(OpenFlipperSettings().value( "Assimp/"+settingPrefix+"/process/debone"                  , false).toBool());
}

void AssimpOptionsWidget::slotMakeDefault()
{
  OpenFlipperSettings().setValue( "Assimp/"+settingPrefix+"/process/calcTangentSpace"        ,   option_assimp_process_calcTangentSpace        ->isChecked()  );
  OpenFlipperSettings().setValue( "Assimp/"+settingPrefix+"/process/joinIdenticalVertices"   ,   option_assimp_process_joinIdenticalVertices   ->isChecked()  );
  OpenFlipperSettings().setValue( "Assimp/"+settingPrefix+"/process/makeLeftHanded"          ,   option_assimp_process_makeLeftHanded          ->isChecked()  );
  OpenFlipperSettings().setValue( "Assimp/"+settingPrefix+"/process/triangulate"             ,   option_assimp_process_triangulate             ->isChecked()  );
  OpenFlipperSettings().setValue( "Assimp/"+settingPrefix+"/process/removeComponent"         ,   option_assimp_process_removeComponent         ->isChecked()  );
  OpenFlipperSettings().setValue( "Assimp/"+settingPrefix+"/process/genNormals"              ,   option_assimp_process_genNormals              ->isChecked()  );
  OpenFlipperSettings().setValue( "Assimp/"+settingPrefix+"/process/genSmoothNormals"        ,   option_assimp_process_genSmoothNormals        ->isChecked()  );
  OpenFlipperSettings().setValue( "Assimp/"+settingPrefix+"/process/splitLargeMeshes"        ,   option_assimp_process_splitLargeMeshes        ->isChecked()  );
  OpenFlipperSettings().setValue( "Assimp/"+settingPrefix+"/process/preTransformVertices"    ,   option_assimp_process_preTransformVertices    ->isChecked()  );
  OpenFlipperSettings().setValue( "Assimp/"+settingPrefix+"/process/limitBoneWeights"        ,   option_assimp_process_limitBoneWeights        ->isChecked()  );
  OpenFlipperSettings().setValue( "Assimp/"+settingPrefix+"/process/validateDataStructure"   ,   option_assimp_process_validateDataStructure   ->isChecked()  );
  OpenFlipperSettings().setValue( "Assimp/"+settingPrefix+"/process/improveCacheLocality"    ,   option_assimp_process_improveCacheLocality    ->isChecked()  );
  OpenFlipperSettings().setValue( "Assimp/"+settingPrefix+"/process/removeRedundantMaterials",   option_assimp_process_removeRedundantMaterials->isChecked()  );
  OpenFlipperSettings().setValue( "Assimp/"+settingPrefix+"/process/fixInfacingNormals"      ,   option_assimp_process_fixInfacingNormals      ->isChecked()  );
  OpenFlipperSettings().setValue( "Assimp/"+settingPrefix+"/process/sortByPType"             ,   option_assimp_process_sortByPType             ->isChecked()  );
  OpenFlipperSettings().setValue( "Assimp/"+settingPrefix+"/process/findDegenerates"         ,   option_assimp_process_findDegenerates         ->isChecked()  );
  OpenFlipperSettings().setValue( "Assimp/"+settingPrefix+"/process/findInvalidData"         ,   option_assimp_process_findInvalidData         ->isChecked()  );
  OpenFlipperSettings().setValue( "Assimp/"+settingPrefix+"/process/genUVCoords"             ,   option_assimp_process_genUVCoords             ->isChecked()  );
  OpenFlipperSettings().setValue( "Assimp/"+settingPrefix+"/process/transformUVCoords"       ,   option_assimp_process_transformUVCoords       ->isChecked()  );
  OpenFlipperSettings().setValue( "Assimp/"+settingPrefix+"/process/findInstances"           ,   option_assimp_process_findInstances           ->isChecked()  );
  OpenFlipperSettings().setValue( "Assimp/"+settingPrefix+"/process/optimizeMeshes"          ,   option_assimp_process_optimizeMeshes          ->isChecked()  );
  OpenFlipperSettings().setValue( "Assimp/"+settingPrefix+"/process/optimizeGraph"           ,   option_assimp_process_optimizeGraph           ->isChecked()  );
  OpenFlipperSettings().setValue( "Assimp/"+settingPrefix+"/process/flipUVs"                 ,   option_assimp_process_flipUVs                 ->isChecked()  );
  OpenFlipperSettings().setValue( "Assimp/"+settingPrefix+"/process/flipWindingOrder"        ,   option_assimp_process_flipWindingOrder        ->isChecked()  );
  OpenFlipperSettings().setValue( "Assimp/"+settingPrefix+"/process/splitByBoneCount"        ,   option_assimp_process_splitByBoneCount        ->isChecked()  );
  OpenFlipperSettings().setValue( "Assimp/"+settingPrefix+"/process/debone"                  ,   option_assimp_process_debone                  ->isChecked()  );
  
  OpenFlipperSettings().setValue( "Core/File/Use"+settingPrefix+"Defaults", true );
}

unsigned int AssimpOptionsWidget::processSteps()
{
  unsigned int result = 0;
  for(auto entry : loadOptions)
  {
    if(entry.first != nullptr)
      result |= entry.first->isChecked()? entry.second : 0;
  }
  return result;
}
