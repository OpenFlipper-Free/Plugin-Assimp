/*===========================================================================*\
*                                                                            *
*                              OpenFlipper                                   *
*      Copyright (C) 2001-2011 by Computer Graphics Group, RWTH Aachen       *
*                           www.openflipper.org                              *
*                                                                            *
*--------------------------------------------------------------------------- *
*  This file is part of OpenFlipper.                                         *
*                                                                            *
*  OpenFlipper is free software: you can redistribute it and/or modify       *
*  it under the terms of the GNU Lesser General Public License as            *
*  published by the Free Software Foundation, either version 3 of            *
*  the License, or (at your option) any later version with the               *
*  following exceptions:                                                     *
*                                                                            *
*  If other files instantiate templates or use macros                        *
*  or inline functions from this file, or you compile this file and          *
*  link it with other files to produce an executable, this file does         *
*  not by itself cause the resulting executable to be covered by the         *
*  GNU Lesser General Public License. This exception does not however        *
*  invalidate any other reasons why the executable file might be             *
*  covered by the GNU Lesser General Public License.                         *
*                                                                            *
*  OpenFlipper is distributed in the hope that it will be useful,            *
*  but WITHOUT ANY WARRANTY; without even the implied warranty of            *
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             *
*  GNU Lesser General Public License for more details.                       *
*                                                                            *
*  You should have received a copy of the GNU LesserGeneral Public           *
*  License along with OpenFlipper. If not,                                   *
*  see <http://www.gnu.org/licenses/>.                                       *
*                                                                            *
\*===========================================================================*/

#include "AssimpPlugin.hh"
#include <Type-Light/ObjectTypes/Light/Light.hh>
#include <Type-Light/ObjectTypes/Light/LightObject.hh>
#include <ObjectTypes/Light/LightNode.hh>
#include <OpenMesh/Core/Geometry/VectorT.hh>
#include <Type-Light/ObjectTypes/Light/PluginFunctionsLight.hh>

#include <ObjectTypes/Camera/Camera.hh>

#include "AssimpPluginCommon.hh"
#include "widgets/AssimpOptionsWidget.hh"
#include "widgets/AssimpLoadOptionsWidget.hh"
#include "widgets/AssimpSaveOptionsWidget.hh"

#include <QMessageBox>

using namespace OpenMesh; 

namespace
{
//helper function to convert aiVector to OpenMeshVector
template<typename T, typename R>
static T toOMVec(R _vec)
{
  T res;
  for(size_t i = 0 ; i < res.size() ; ++i )
    res[i] = _vec[i];
  return res;
}

}

// static variable to conveniently iterate over all texture types of assimp

std::vector<aiTextureType> allTextureTypes = {aiTextureType_UNKNOWN      ,
                                              aiTextureType_AMBIENT      ,
                                              aiTextureType_DIFFUSE      ,
                                              aiTextureType_DISPLACEMENT ,
                                              aiTextureType_EMISSIVE     ,
                                              aiTextureType_HEIGHT       ,
                                              aiTextureType_LIGHTMAP     ,
                                              aiTextureType_NONE         ,
                                              aiTextureType_NORMALS      ,
                                              aiTextureType_OPACITY      ,
                                              aiTextureType_REFLECTION   ,
                                              aiTextureType_SHININESS    ,
                                              aiTextureType_SPECULAR      };

AssimpPlugin::AssimpPlugin()
  :
  loadOptions_(nullptr),
  saveOptions_(nullptr),
  options_(),
  type_(DATA_UNKNOWN),
  cancel_(false)
{
  processSteps = aiProcess_FindDegenerates | aiProcess_JoinIdenticalVertices | 0;
}

//--------------------------------------------------------------------------

void AssimpPlugin::initializePlugin() {

  QString info =
    " Copyright (c) 2006-2012 assimp team<br>                                                "
    " All rights reserved.<br>                                                               "
    " Redistribution and use in source and binary forms, with or without modification, are   "
    " permitted provided that the following conditions are met: <br>                         "
    " <br>                                                                                   "
    " Redistributions of source code must retain the above copyright notice, this list of    "
    " conditions and the following disclaimer.<br>                                           "
    " <br>                                                                                   "
    " Redistributions in binary form must reproduce the above copyright notice, this list    "
    " of conditions and the following disclaimer in the documentation and/or other           "
    " materials provided with the distribution.<br>                                          "
    " <br>                                                                                   "
    " Neither the name of the assimp team nor the names of its contributors may be used to   "
    " endorse or promote products derived from this software without specific prior written  "
    " permission.<br>                                                                        "
    " <br>                                                                                   "
    " THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS \"AS IS\" AND ANY  "
    " EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF"
    " MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL "
    " THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,    "
    " SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,           "
    " PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS"
    " INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,      "
    " STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF"
    " THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.           "
    " <br>                                                                                   ";

    emit addAboutInfo(info,"File-Assimp Plugin");
  
  connect(this, SIGNAL(showConfirmationDialog(const QString&, const QString&)), this, SLOT(slotShowConfirmationDialog(const QString&, const QString&)), Qt::BlockingQueuedConnection);  
}

//--------------------------------------------------------------------------

int AssimpPlugin::convertAiSceneToOpenMesh(const aiScene *_scene, QString _objectName) {
  int returnId = -1;
  std::vector<int> ids;
  embeddedTextures_.clear();
  loadedImages_.clear();
  
  if (_scene->mNumMeshes == 0)
    emit log(LOGWARN, tr("aiScene contains no meshes"));
  
  // TODO: change the confirmation dialog to a selection dialog where the user can select how to deal with multiple materials
  if(_scene->mNumMaterials > 1 && OpenFlipper::Options::gui())
  {
    if(!emit showConfirmationDialog(tr("Multiple Materials in file"), 
                                    tr("The File you are trying to load contains %1 materials. "
                                       "Assimp creates a separate Mesh for each material, which may cause performance issues.\n\n "
                                       "Do you want to continue loading the file?").arg(_scene->mNumMaterials)))
      return returnId;
  }
  
  //load textures first if requested, 
  //as they are referenced by the materials
  if(options_.loadTexturesOption_)
    loadTexturesFromScene(_scene);
  
  // we always want to load meshes
  loadMeshesFromScene(_scene, ids, _objectName);
  returnId = ids[0];
  
#ifdef ENABLE_CAMERA_SUPPORT
  if(options_.loadCamerasOption_)
    loadCamerasFromScene(_scene);
#endif
  
#ifdef ENABLE_LIGHT_SUPPORT
  if(options_.loadLightsOption_)
    loadLightsFromScene(_scene);
#endif
  
#ifdef ENABLE_SKELETON_SUPPORT
  if(options_.loadAnimationsOption_)
    loadAnimationsFromScene(_scene);
#endif

  //group OpenFlipper objects, if the file contains multiple meshes
  if (_scene->mNumMeshes > 1) {
    bool dataControlExists = false;
    pluginExists( "datacontrol", dataControlExists );

    if ( dataControlExists ){
      returnId = RPC::callFunctionValue<int>("datacontrol","groupObjects", ids, _objectName);
    }
  }

  return returnId;
}

//--------------------------------------------------------------------------

bool AssimpPlugin::convertOpenMeshToAiScene(aiScene *_scene, BaseObjectData *_object) {
  _scene->mMeshes = new aiMesh*[1];
  _scene->mMeshes[0] = new aiMesh();
  _scene->mNumMeshes = 1;
  _scene->mRootNode = new aiNode();
  _scene->mRootNode->mNumChildren = 0;
  _scene->mRootNode->mNumMeshes = 1;
  _scene->mRootNode->mMeshes = new unsigned int[1];
  _scene->mRootNode->mMeshes[0] = 0;

  // assimp requires at least one material
  _scene->mMaterials = new aiMaterial*[1];
  _scene->mMaterials[0] = new aiMaterial();
  _scene->mNumMaterials = 1;

  if ( _object->dataType( DATA_POLY_MESH ) ) {
    return convertPolyMeshToAiMesh(dynamic_cast<PolyMeshObject*>(_object)->mesh(), _scene->mMeshes[0]);
  }
  else if ( _object->dataType( DATA_TRIANGLE_MESH ) ) {
    return convertTriMeshToAiMesh(dynamic_cast<TriMeshObject*>(_object)->mesh(), _scene->mMeshes[0]);
  }
  else {
    emit log(LOGERR, tr("Unable to save (object is not a compatible mesh type)"));
    return false;
  }
}

//--------------------------------------------------------------------------

void AssimpPlugin::convertAiMeshToPolyMesh(PolyMesh *_polyMesh, aiMesh *_mesh) {
  mapVertices(_polyMesh, _mesh);

  std::vector<OpenMesh::VertexHandle> vhandles;
  for (unsigned int i = 0; i < _mesh->mNumFaces; ++i) {
    aiFace& face = _mesh->mFaces[i];
    for (unsigned int j = 0; j < face.mNumIndices; ++j) {
      vhandles.push_back(vertexHandles_[face.mIndices[j]]);
      if (_mesh->HasNormals() && options_.loadNormalsOption_) {
        aiVector3D& aiNormal = _mesh->mNormals[face.mIndices[j]];
        _polyMesh->set_normal(vhandles.back(), ACG::Vec3d(aiNormal.x, aiNormal.y, aiNormal.z));
      }
    }
    _polyMesh->add_face(vhandles);
    vhandles.clear();
  }

  if (!_mesh->HasNormals())
    _polyMesh->update_normals();
  else
    _polyMesh->update_face_normals();
}

//--------------------------------------------------------------------------

void AssimpPlugin::convertAiMeshToTriMesh(TriMesh *_triMesh, aiMesh *_mesh) {
  mapVertices(_triMesh, _mesh);

  std::vector<OpenMesh::VertexHandle> vhandles;
  for (unsigned int i = 0; i < _mesh->mNumFaces; ++i) {
    aiFace& face = _mesh->mFaces[i];
    for (unsigned int j = 0; j < face.mNumIndices; ++j) {
      vhandles.push_back(vertexHandles_[face.mIndices[j]]);
      if (_mesh->HasNormals() && options_.loadNormalsOption_) {
        aiVector3D& aiNormal = _mesh->mNormals[face.mIndices[j]];
        _triMesh->set_normal(vhandles.back(), ACG::Vec3d(aiNormal.x, aiNormal.y, aiNormal.z));
      }
    }

    _triMesh->add_face(vhandles);
    vhandles.clear();
  }

  if (!_mesh->HasNormals())
    _triMesh->update_normals();
  else
    _triMesh->update_face_normals();
}

//--------------------------------------------------------------------------

bool AssimpPlugin::convertPolyMeshToAiMesh(PolyMesh *_polyMesh, aiMesh *_mesh) {
  _mesh->mPrimitiveTypes = aiPrimitiveType_POLYGON;
  _mesh->mNumVertices = _polyMesh->n_vertices();
  _mesh->mNumFaces = _polyMesh->n_faces();

  _mesh->mVertices = new aiVector3D[_mesh->mNumVertices];
  _mesh->mNormals = new aiVector3D[_mesh->mNumVertices];
  _mesh->mFaces = new aiFace[_mesh->mNumFaces];

  std::map<OpenMesh::VertexHandle, int> vertexHandles;
  int i = 0;
  for (PolyMesh::ConstVertexIter v_it = _polyMesh->vertices_begin(); v_it != _polyMesh->vertices_end(); ++v_it, ++i) {
      ACG::Vec3d pos = _polyMesh->point(*v_it);
      ACG::Vec3d normal = _polyMesh->normal(*v_it);
      _mesh->mVertices[i] = aiVector3D(pos[0], pos[1], pos[2]);
      _mesh->mNormals[i] = aiVector3D(normal[0], normal[1], normal[2]);
      vertexHandles[*v_it] = i;
  }

  i = 0;
  for (PolyMesh::ConstFaceIter f_it = _polyMesh->faces_begin(); f_it != _polyMesh->faces_end(); ++f_it, ++i) {
    int nVertices = 0;
    for (PolyMesh::ConstFaceVertexIter fv_it = _polyMesh->fv_iter(*f_it); fv_it.is_valid(); ++fv_it) {
      ++nVertices;
    }

    _mesh->mFaces[i].mNumIndices = nVertices;
    _mesh->mFaces[i].mIndices = new unsigned int[nVertices];
    int j = 0;
    for (PolyMesh::ConstFaceVertexIter fv_it = _polyMesh->fv_iter(*f_it); fv_it.is_valid(); ++fv_it, ++j) {
      _mesh->mFaces[i].mIndices[j] = vertexHandles[*fv_it];
    }
  }

  return true;
}

//--------------------------------------------------------------------------

bool AssimpPlugin::convertTriMeshToAiMesh(TriMesh *_triMesh, aiMesh *_mesh) {
  _mesh->mPrimitiveTypes = aiPrimitiveType_TRIANGLE;
  _mesh->mNumVertices = _triMesh->n_vertices();
  _mesh->mNumFaces = _triMesh->n_faces();

  _mesh->mVertices = new aiVector3D[_mesh->mNumVertices];
  _mesh->mNormals = new aiVector3D[_mesh->mNumVertices];
  _mesh->mFaces = new aiFace[_mesh->mNumFaces];

  std::map<OpenMesh::VertexHandle, int> vertexHandles;
  int i = 0;
  for (TriMesh::ConstVertexIter v_it = _triMesh->vertices_begin(); v_it != _triMesh->vertices_end(); ++v_it, ++i) {
      ACG::Vec3d pos = _triMesh->point(*v_it);
      ACG::Vec3d normal = _triMesh->normal(*v_it);
      _mesh->mVertices[i] = aiVector3D(pos[0], pos[1], pos[2]);
      _mesh->mNormals[i] = aiVector3D(normal[0], normal[1], normal[2]);
      vertexHandles[*v_it] = i;
  }

  i = 0;
  for (TriMesh::ConstFaceIter f_it = _triMesh->faces_begin(); f_it != _triMesh->faces_end(); ++f_it, ++i) {
    _mesh->mFaces[i].mNumIndices = 3;
    _mesh->mFaces[i].mIndices = new unsigned int[3];
    int j = 0;
    for (PolyMesh::ConstFaceVertexIter fv_it = _triMesh->fv_iter(*f_it); fv_it.is_valid(); ++fv_it, ++j) {
      _mesh->mFaces[i].mIndices[j] = vertexHandles[*fv_it];
    }
  }

  return true;
}

//--------------------------------------------------------------------------

void AssimpPlugin::loadAnimationsFromScene(const aiScene* _scene)
{
  for(unsigned int i = 0 ; !cancel_ && i < _scene->mNumAnimations ; ++i)
  {
    //TODO: implement animation loading
  }
}

//--------------------------------------------------------------------------

void AssimpPlugin::loadLightsFromScene(const aiScene* _scene)
{
  
  for(unsigned int i = 0 ; !cancel_ && i < _scene->mNumLights ; ++i)
  {    
    //only support point lights for now
    //TODO: support more light types
    if(_scene->mLights[i]->mType == aiLightSource_POINT)
    {
      int id;
      BaseObjectData* object;
      emit addEmptyObject(DATA_LIGHT, id);
      if(!PluginFunctions::getObject(id, object))
      {
        emit log(LOGERR, "could not add new light object");
        return;
      }
      LightObject* light = PluginFunctions::lightObject(object);      
      if(light)
      {
        light->setName(_scene->mLights[i]->mName.C_Str());   
        //compute the absolute light position
        aiNode* node = _scene->mRootNode->FindNode(_scene->mLights[i]->mName);
        //TODO: check if position is always corrent, e.g. if multiple nodes are used,
        //and for file formats other than blender files
        aiVector3D position = node->mTransformation * _scene->mLights[i]->mPosition;
        LightSource* source = PluginFunctions::lightSource(light);   
        
        source->position(toOMVec<Vec3d>(position));
        source->constantAttenuation(_scene->mLights[i]->mAttenuationConstant);
        source->linearAttenuation(_scene->mLights[i]->mAttenuationLinear);
        source->quadraticAttenuation(_scene->mLights[i]->mAttenuationQuadratic); 
        source->ambientColor(color_cast<Vec4f>(toOMVec<Vec3f>(_scene->mLights[i]->mColorAmbient)));
        source->diffuseColor(color_cast<Vec4f>(toOMVec<Vec3f>(_scene->mLights[i]->mColorDiffuse)));
        source->specularColor(color_cast<Vec4f>(toOMVec<Vec3f>(_scene->mLights[i]->mColorSpecular))); 
        source->enable();
        emit updatedObject(id, UPDATE_ALL);
      }
    }
    else
      emit log(LOGWARN, "unsupported light type");
  }
}

//--------------------------------------------------------------------------

void AssimpPlugin::loadCamerasFromScene(const aiScene* _scene)
{
  for(unsigned int i = 0 ; !cancel_ && i < _scene->mNumCameras ; ++i)
  {    
    int id;
    emit addEmptyObject(DATA_CAMERA,id);
    CameraObject* cameraObject = PluginFunctions::cameraObject(id);
    if(cameraObject)
    {
      CameraNode* cameraNode = cameraObject->cameraNode();
      cameraObject->setName(_scene->mCameras[i]->mName.C_Str());
      cameraNode->setNearPlane(_scene->mCameras[i]->mClipPlaneNear);
      cameraNode->setFarPlane(_scene->mCameras[i]->mClipPlaneFar);
      
      //todo retrieve matrices
      //cameraNode->
    }
    emit updatedObject(id,UPDATE_ALL);
  }
}

//--------------------------------------------------------------------------

void AssimpPlugin::loadTexturesFromScene(const aiScene* _scene)
{ 
  //load embedded textures
    for(unsigned int j = 0 ;!cancel_ && j < _scene->mNumTextures ; ++j)
    {
      aiTexture* tex = _scene->mTextures[j];                              
      QImage img;
      img = QImage::fromData(reinterpret_cast<uchar *>(tex->pcData),tex->mWidth);
      embeddedTextures_.push_back(std::make_pair(QString("EmbeddedTexture_%1").arg(j).toStdString(),img));
      
      //if you encounter cases where mHeight if tex is not 0, copy them via memcopy, or use the following code block.
      /*uint sz = tex->mWidth * tex->mHeight;
                  QByteArray data;
                  data.resize(sz * 4);
                  uchar *p = reinterpret_cast<uchar *>(data.data());
                  for (uint j = 0; j < sz; ++j) {
                      *p++ = tex->pcData[j].r;
                      *p++ = tex->pcData[j].g;
                      *p++ = tex->pcData[j].b;
                      *p++ = tex->pcData[j].a;
                  }
                  img = QImage(reinterpret_cast<const uchar *>(data.constData()), tex->mWidth, tex->mHeight, QImage::Format_RGBA8888);
                  img.detach();*/
    }
}

//--------------------------------------------------------------------------

std::vector<std::pair<std::string,QImage>> AssimpPlugin::loadTexturesFromMaterial(const aiMaterial* _material, aiTextureType _textureType)
{
  std::vector<std::pair<std::string, QImage>> result;
  aiString path;
  for(unsigned int i =  0 ; i < _material->GetTextureCount(_textureType) ; i++ )
  {
    //retrieve texture path embedded textures start with * followed by the texture index e.g. *0
    _material->GetTexture(_textureType, i, &path, nullptr, nullptr, nullptr, nullptr, nullptr);    
    if(path.length > 0)
    {
    if(path.C_Str()[0]=='*') // load the embedded texture
    {
      const char* substr = path.C_Str();
      ++substr;             //skip leading * character
      result.push_back(embeddedTextures_[std::atoi(substr)]);
    }
    else                     // load the external texture
    {
      QImage img(QString(path.C_Str()));
      if(!img.isNull())
        result.push_back(std::make_pair(path.C_Str(),img));        
      else
        emit log(LOGWARN, QString("could not load texture: %1").arg(path.C_Str()));      
    }
    }
    path.Clear();
  }
  return result;
}

//--------------------------------------------------------------------------

void AssimpPlugin::loadMaterial(const aiScene* _scene, MaterialNode* _materialNode, unsigned int _materialIndex)
{
  if(_materialIndex >= _scene->mNumMaterials)
  {
    emit log(LOGERR, "Unable to load material, index is out of bounds.");
    return;
  }
  
  //load properties from the material
  //TODO: load more properties if applicable
  aiMaterial* mat = _scene->mMaterials[_materialIndex];
  for(unsigned int i = 0; i < mat->mNumProperties ; ++i)
  {
    aiColor3D color;
    float coefficient;
    if(mat->Get(AI_MATKEY_COLOR_DIFFUSE,color)==AI_SUCCESS)
      _materialNode->set_diffuse_color(color_cast<Vec4f>(toOMVec<Vec3f>(color)));
    if(mat->Get(AI_MATKEY_COLOR_AMBIENT, color)==AI_SUCCESS)
      _materialNode->set_ambient_color(color_cast<Vec4f>(toOMVec<Vec3f>(color)));
    if(mat->Get(AI_MATKEY_COLOR_EMISSIVE, color)==AI_SUCCESS)
      _materialNode->set_emission(color_cast<Vec4f>(toOMVec<Vec3f>(color)));
    if(mat->Get(AI_MATKEY_COLOR_SPECULAR, color)==AI_SUCCESS)
      _materialNode->set_specular_color(color_cast<Vec4f>(toOMVec<Vec3f>(color)));
    if(mat->Get(AI_MATKEY_REFLECTIVITY, coefficient) == AI_SUCCESS)
      _materialNode->set_reflectance(coefficient);
    if(mat->Get(AI_MATKEY_SHININESS, coefficient) == AI_SUCCESS)
      _materialNode->set_shininess(coefficient);    
  }
  
  //load textures from this material if requested
  if(options_.loadTexturesOption_)
  {
    std::vector<std::pair<std::string, QImage>> materialImages;
    for(aiTextureType type : allTextureTypes)
    {
      //gater all textures in a single vector
      auto images = loadTexturesFromMaterial(mat,type);
      materialImages.insert(materialImages.end(),images.begin(), images.end());
    }
    //add entry for the material
    loadedImages_[_materialIndex] = materialImages;
  }  
}

//--------------------------------------------------------------------------

void AssimpPlugin::loadMeshesFromScene(const aiScene* _scene, std::vector<int> & _outIds, QString& _objectName)
{
  // create a new OpenFlipper object for each mesh in the scene, as long as the user did not cancel loading 
  for (unsigned int i = 0; i < _scene->mNumMeshes && !cancel_; ++i) {
    int objectId = -1;
    if(type_ == DATA_UNKNOWN)
    {
      if(_scene->mMeshes[i]->mPrimitiveTypes & aiPrimitiveType_POLYGON)
        emit addEmptyObject(DATA_POLY_MESH, objectId);
      else
        emit addEmptyObject(DATA_TRIANGLE_MESH, objectId);
    }
    else
      emit addEmptyObject(type_, objectId);

    BaseObject* object(0);
    if(!PluginFunctions::getObject( objectId, object )) {
      emit log(LOGERR, tr("Could not create new object!"));
      _outIds.push_back(-1);
      return;
    }

    object->setName(_objectName);

    PolyMeshObject* polyMeshObj = dynamic_cast< PolyMeshObject* > (object);
    TriMeshObject*  triMeshObj  = dynamic_cast< TriMeshObject*  > (object);    

    if (polyMeshObj) {
      convertAiMeshToPolyMesh(polyMeshObj->mesh(), _scene->mMeshes[i]);      
      loadMaterial(_scene, polyMeshObj->materialNode(),_scene->mMeshes[i]->mMaterialIndex);            
    } else if (triMeshObj) {
      convertAiMeshToTriMesh(triMeshObj->mesh(), _scene->mMeshes[i]);
      loadMaterial(_scene, triMeshObj->materialNode(),_scene->mMeshes[i]->mMaterialIndex);            
    }
    //add the textures if requested
    if(options_.loadTexturesOption_)
    {
      for(auto entry : loadedImages_[_scene->mMeshes[i]->mMaterialIndex])
      {
        emit addTexture    (QString(entry.first.c_str()), entry.second, 2, objectId);  //only support 2d textures for now TODO: add more
        emit switchTexture (QString(entry.first.c_str()), objectId );
        //TODO: investigate why new textures are not rendered. probably because texture index property is not set
        //emit setTextureMode(QString(entry.first.c_str()),"indexProperty=OriginalTexIndexMapping", objectId );
      }
    }
    _outIds.push_back(object->id());
    emit updatedObject(objectId, UPDATE_ALL);
    emit openedFile( object->id() );
  }
}

//--------------------------------------------------------------------------

DataType AssimpPlugin::supportedType() {
  DataType type = DATA_POLY_MESH | DATA_TRIANGLE_MESH | DATA_GROUP;
  return type;
}

//--------------------------------------------------------------------------

QString AssimpPlugin::getSaveFilters() {
  return QString( tr("Alias/Wavefront ( *.obj );;Collada ( *.dae );;Stereolithography files ( *.stl );;Polygon File Format files ( *.ply )" ) );
}

//--------------------------------------------------------------------------

QString AssimpPlugin::getLoadFilters() {
  return QString( tr("Alias/Wavefront ( *.obj );;AutoCAD DXF ( *.dxf );;Collada ( *.dae );;Stereolithography files ( *.stl );;Polygon File Format files ( *.ply );;Blender 3D( *.blend );;3ds Max 3DS ( *.3ds );;3ds Max ASE( *.ase );;Industry Foundation Classes ( *.ifc );;XGL ( *.xgl *.zgl );;Lightwave ( *.lwo );;Lightwave Scene ( *.lws );;Modo ( *.lxo );;DirectX X ( *.x );;AC3D ( *.ac );;Milkshape 3D ( *.ms3d );;TrueSpace ( *.cob *.scn );;Filmbox ( *.fbx )") );
}

//--------------------------------------------------------------------------

QWidget *AssimpPlugin::saveOptionsWidget(QString) {
  if (saveOptions_ == nullptr){
      //generate widget
      saveOptions_ = new AssimpSaveOptionsWidget(options_);      
  }

  return saveOptions_;
}

//--------------------------------------------------------------------------

QWidget *AssimpPlugin::loadOptionsWidget(QString) {
  if (loadOptions_ == 0){
      //generate widget
    loadOptions_ = new AssimpLoadOptionsWidget(options_);
  }

  return loadOptions_;
}

//--------------------------------------------------------------------------

int AssimpPlugin::loadObject(QString _filename) {
  Assimp::Importer importer;
  cancel_ = false;
  
  const aiScene* scene = NULL;
  if(OpenFlipper::Options::gui() && loadOptions_ != nullptr)
    processSteps = loadOptions_->processSteps();
  scene = importer.ReadFile(_filename.toStdString(), processSteps);

  if (!scene) {
    emit log(LOGERR, tr(importer.GetErrorString()));
    return -1;
  }
  
  if(OpenFlipper::Options::gui() && loadOptions_ != nullptr)
  {
    switch(options_.triMeshHandling_)
    {
      case TYPEAUTODETECT :
          type_ = DATA_UNKNOWN;
          break;
      case TYPEPOLY :
          type_ = DATA_POLY_MESH;
         break;
      case TYPETRIANGLE :
          type_ = DATA_TRIANGLE_MESH;
         break;
      case TYPEASK :
          QMessageBox msgBox;
          QPushButton *detectButton = msgBox.addButton(tr("Auto-Detect"), QMessageBox::ActionRole);
          QPushButton *triButton    = msgBox.addButton(tr("Open as triangle mesh"), QMessageBox::ActionRole);
          QPushButton *polyButton   = msgBox.addButton(tr("Open as poly mesh"), QMessageBox::ActionRole);
          msgBox.setWindowTitle( tr("Mesh types in file") );
          msgBox.setText( tr("You are about to open a file containing one or more mesh types. \n\n Which mesh type should be used?") );
          msgBox.setDefaultButton( detectButton );
          msgBox.exec();
          if (msgBox.clickedButton() == triButton)
            type_ = DATA_TRIANGLE_MESH;
          else if (msgBox.clickedButton() == polyButton)
            type_ = DATA_POLY_MESH;
         break;
    }
  }

  QFileInfo f(_filename);
  return convertAiSceneToOpenMesh(scene, f.fileName());
}

//--------------------------------------------------------------------------

int AssimpPlugin::loadObject(QString _filename, DataType _type) {
  if (_type != DATA_GROUP) //if equal => autodetect data
    type_ = _type;
  return loadObject(_filename);
}

//--------------------------------------------------------------------------

bool AssimpPlugin::saveObject(int _id, QString _filename) {
  BaseObjectData* object;
  PluginFunctions::getObject(_id,object);

  if (!object) {
    emit log(LOGERR, tr("Could not get the object with the given id"));
    return false;
  }

  object->setFromFileName(_filename);
  object->setName(object->filename());

  aiScene scene;

  if (!convertOpenMeshToAiScene(&scene, object))
    return false;

  Assimp::Exporter exporter;

  QFileInfo f(_filename);

  std::string formatId = (f.suffix() == "dae") ? "collada" : f.suffix().toStdString();
  bool ok = exporter.Export(&scene, formatId, _filename.toStdString()) == AI_SUCCESS;
  if (!ok)
    emit log(LOGERR, exporter.GetErrorString());

  return ok;
}

//--------------------------------------------------------------------------

bool AssimpPlugin::slotShowConfirmationDialog(const QString& _title, const QString& _message)
{
  QMessageBox msgBox;
  QPushButton *cancelButton       =   msgBox.addButton(tr("Cancel"), QMessageBox::ActionRole);
  /*QPushButton *confirmButton    =*/ msgBox.addButton(tr("Continue"), QMessageBox::ActionRole);    
  msgBox.setWindowTitle( _title );
  msgBox.setText( _message );
  msgBox.setDefaultButton( cancelButton );
  msgBox.exec();
  if (msgBox.clickedButton() == cancelButton)
    return false;
  return true;
}

//--------------------------------------------------------------------------

void AssimpPlugin::canceledJob(QString _jobId)
{
  if(_jobId ==  "Loading File" || _jobId == "Loading Files")
    cancel_ = true;
}
